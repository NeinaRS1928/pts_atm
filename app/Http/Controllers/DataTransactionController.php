<?php

namespace App\Http\Controllers;

use App\Models\DataTransaction;
use Illuminate\Http\Request;

class DataTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DataTransaction  $dataTransaction
     * @return \Illuminate\Http\Response
     */
    public function show(DataTransaction $dataTransaction)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DataTransaction  $dataTransaction
     * @return \Illuminate\Http\Response
     */
    public function edit(DataTransaction $dataTransaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\DataTransaction  $dataTransaction
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, DataTransaction $dataTransaction)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DataTransaction  $dataTransaction
     * @return \Illuminate\Http\Response
     */
    public function destroy(DataTransaction $dataTransaction)
    {
        //
    }
}
